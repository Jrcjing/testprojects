//
//  Player.h
//  StoryboardTest
//
//  Created by Lisa on 15/6/1.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *game;
@property (nonatomic,assign) int rating;
@end
