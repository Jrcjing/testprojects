//
//  AppDelegate.h
//  StoryboardTest
//
//  Created by Lisa on 15/5/28.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"
#import "PlayersViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) NSMutableArray *players;

@end

