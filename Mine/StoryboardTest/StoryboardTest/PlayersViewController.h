//
//  PlayersViewController.h
//  StoryboardTest
//
//  Created by Lisa on 15/6/1.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayersViewController : UITableViewController
@property (nonnull,strong) NSMutableArray *players;
@end
