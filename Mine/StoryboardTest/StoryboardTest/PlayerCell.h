//
//  PlayerCell.h
//  StoryboardTest
//
//  Created by Lisa on 15/6/1.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *gameLabel;
@property (nonatomic,weak) IBOutlet UIImageView *ratingImageView;
@end
