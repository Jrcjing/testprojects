//
//  ViewController.m
//  ContextTest
//
//  Created by Lisa on 15/4/7.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "ViewController.h"
#import "HomeView.h"

@interface ViewController ()
{
    HomeView *_theView;
}
@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _theView = [[HomeView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_theView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
