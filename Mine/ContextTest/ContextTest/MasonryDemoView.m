//
//  MasonryView.m
//  ContextTest
//
//  Created by Lisa on 15/5/13.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "MasonryDemoView.h"
#import "MMPlaceHolder.h"

@implementation MasonryDemoView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        
        WS(weakSelf);
        
        UIView *viewcenter = [UIView new];
        [self addSubview:viewcenter];
        viewcenter.backgroundColor = [UIColor blackColor];
        [viewcenter showPlaceHolder];
        [viewcenter mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(weakSelf);
            make.size.mas_equalTo(CGSizeMake(300, 300));
        }];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
