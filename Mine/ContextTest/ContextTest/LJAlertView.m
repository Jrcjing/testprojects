//
//  AlertView.m
//  ContextTest
//
//  Created by Lisa on 15/4/15.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "LJAlertView.h"
#import "UITool.h"
#import "IQKeyboardReturnKeyHandler.h"


const UIWindowLevel UIWindowLevelLJAlertView = 999;
const UIWindowLevel UIWindowLevelLJAlertViewBackground = UIWindowLevelLJAlertView - 1;

@class LJAlertViewBackground;
static LJAlertViewBackground *__static_alert_background_window;

@interface LJAlertViewBackground : UIWindow
@end

@implementation LJAlertViewBackground
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.opaque = NO;
        self.windowLevel = UIWindowLevelLJAlertViewBackground;
        
        RootViewController *controller = [[RootViewController alloc] init];
        controller.view.backgroundColor = [UIColor clearColor];
        self.rootViewController = controller;

        [self makeKeyAndVisible];
    }
    return self;
}

+ (void)showBackground
{
    if (!__static_alert_background_window) {
        __static_alert_background_window = [[LJAlertViewBackground alloc] initWithFrame:kBounds_Screen];
        [__static_alert_background_window makeKeyAndVisible];
        __static_alert_background_window.alpha = 0;
        [UIView animateWithDuration:0.3
                         animations:^{
                             __static_alert_background_window.alpha = 1;
                         }];
    }
}

+ (void)hideBackgroundAnimated:(BOOL)animated
{
    if (!animated) {
        [__static_alert_background_window removeFromSuperview];
        __static_alert_background_window = nil;
        return;
    }
    [UIView animateWithDuration:0.3
                     animations:^{
                         __static_alert_background_window.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [__static_alert_background_window removeFromSuperview];
                         __static_alert_background_window = nil;
                     }];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    size_t locationsCount = 2;
    CGFloat locations[2] = {0.0f, 1.0f};
    CGFloat colors[8] = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.75f};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, locations, locationsCount);
    CGColorSpaceRelease(colorSpace);
    
    CGPoint center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    CGFloat radius = MIN(self.frame.size.width, self.frame.size.height) ;
    CGContextDrawRadialGradient (context, gradient, center, 0, center, radius, kCGGradientDrawsAfterEndLocation);
    CGGradientRelease(gradient);
}

@end

@interface LJAlertViewController : RootViewController
@property (nonatomic,strong) LJAlertView *alertView;
@end

@implementation LJAlertViewController
- (void)loadView{
    [super loadView];
    self.view = _alertView;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [_alertView setUp];
}
@end



@interface LJAlertView()
{
    UIWindow *_alertWindow;
    UIView   *_mainView;
    UIView   *_contentView;
    UIView   *_itemsView;
    UIView   *_cancelView;
    
    NSString *_title;
    NSString *_message;
    NSMutableArray  *_cancelBtns;
    
    CATextLayer  *_titleLayer;
    CATextLayer  *_messageLayer;
    
    IQKeyboardReturnKeyHandler *returnKeyHandler;
}
@end

@implementation LJAlertView

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelBtns:(NSArray *)cancelBtns{
    self = [super initWithFrame:kRect_RootView];
    if(self){
        _title = title;
        _message = message;
        _cancelBtns = [NSMutableArray arrayWithArray:cancelBtns];
    }
    return self;
}

- (void)setUp{
    [self setUpMainView];
    [self setUpContentView];
    [self setUpTitleLabel];
    [self setUpMessageLabel];
    [self setUpItemsView];
    [self setUpCancelView];
    [self updateSubviews];
}

-(void)reloadData{
    [self setUpTitleLabel];
    [self setUpMessageLabel];
    [self setUpItemsView];
    [self setUpCancelView];
    [self updateItemsView];
}

- (void)setUpMainView{
    _mainView = [[UIView alloc] initWithFrame:self.bounds];
    _mainView.backgroundColor = [UIColor clearColor];
    [self addSubview:_mainView];
}

- (void)setUpContentView{
    _contentView = [[UIView alloc] initWithFrame:_mainView.bounds];
    _contentView.backgroundColor = [UIColor whiteColor];
    _contentView.layer.cornerRadius = 20;
    _contentView.layer.shouldRasterize = YES;
    [_mainView addSubview:_contentView];
}

- (void)setUpCancelView{
    if(!_cancelBtns.count) return;
    
    _cancelView = [[UIView alloc] initWithFrame:_mainView.bounds];
    _cancelView.backgroundColor = [UIColor clearColor];
    [_mainView addSubview:_cancelView];
    
    NSInteger btnCount = _cancelBtns.count;
    CGFloat width = (kAlertView_Width - (btnCount - 1)*kAlertView_Space_Btns)/btnCount;
    for (int i=0; i<_cancelBtns.count; i++) {
        NSString *title = [_cancelBtns objectAtIndex:i];
        CGFloat originX = (width + kAlertView_Space_Btns)*i;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn.layer setCornerRadius:10.0];
        [btn.layer setShouldRasterize:YES];
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setTitle:title forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [btn setFrame:CGRectMake(originX, 0, width, kAlertView_Height_Cancel)];
        [btn setTag:kAlertView_CancelTag + i];
        [btn addTarget:self action:@selector(cancelBtnsDidSelected:) forControlEvents:UIControlEventTouchUpInside];
        [_cancelView addSubview:btn];
    }
}

- (void)cancelBtnsDidSelected:(UIButton *)btn{
    if(_delegate && [_delegate respondsToSelector:@selector(cancelBtnDidSelected:atIndex:inAlertView:)]){
        [_delegate cancelBtnDidSelected:btn atIndex:btn.tag - kAlertView_CancelTag inAlertView:self];
    }else{
        [self dismiss];
    }
}

- (void)setUpTitleLabel{
    if (_title) {
        if (!_titleLayer) {
            _titleLayer = [UITool createTextLayerWithFont:kAlertView_Font_Title];
            [_titleLayer setWrapped:YES];
            [_titleLayer setAlignmentMode:kCAAlignmentCenter];
            [_contentView.layer addSublayer:_titleLayer];
        }
        CGFloat width = kAlertView_Width - 2*kAlertView_MarginX;
        NSMutableAttributedString *attributedStr = [UITool attributedStringWithText:_title textColor:[UIColor blackColor] font:kAlertView_Font_Title];
        CGSize size = [UITool sizeWithAttributeString:attributedStr font:kAlertView_Font_Title boundingRectWithSize:CGSizeMake(width, MAXFLOAT)];
        
        [_titleLayer setString:attributedStr];
        [_titleLayer setFrame:CGRectMake(0, 0, size.width, size.height)];
        [_titleLayer setPosition:CGPointMake(kAlertView_Width/2, kAlertView_MarginY + size.height/2)];
    } else {
        [_titleLayer removeFromSuperlayer];
        _titleLayer = nil;
    }
}

- (void)setUpMessageLabel{
    if(_message){
        if (!_messageLayer) {
            _messageLayer = [UITool createTextLayerWithFont:kAlertView_Font_Merssage];
            [_messageLayer setWrapped:YES];
            [_messageLayer setAlignmentMode:kCAAlignmentCenter];
            [_contentView.layer addSublayer:_messageLayer];
        }
        CGFloat width = kAlertView_Width - 2*kAlertView_MarginX;
        
        NSMutableAttributedString *attributedStr = [UITool attributedStringWithText:_message textColor:[UIColor grayColor] font:kAlertView_Font_Merssage];
        CGSize size = [UITool sizeWithAttributeString:attributedStr font:kAlertView_Font_Merssage boundingRectWithSize:CGSizeMake(width, MAXFLOAT)];
        [_messageLayer setString:attributedStr];
        [_messageLayer setFrame:CGRectMake(0, 0, size.width, size.height)];
        [_messageLayer setPosition:CGPointMake(kAlertView_Width/2, (_titleLayer?(_titleLayer.frame.origin.y + _titleLayer.frame.size.height + kAlertView_Space_TitleMessage):kAlertView_MarginY) + size.height/2)];
    }else{
        [_messageLayer removeFromSuperlayer];
        _messageLayer = nil;
    }
}

- (void)setUpItemsView{
    if(!_itemsView){
        if(!_dataSource) return;
        
        _itemsView = [[UIView alloc] initWithFrame:self.bounds];
        [_contentView addSubview:_itemsView];
        
        [self updateItemsView];
    }else{
        if(!_dataSource){
            for(UIView *subView in _itemsView.subviews){
                [subView removeFromSuperview];
            }
            [_itemsView removeFromSuperview];
            _itemsView = nil;
            return;
        }
        
        [self updateItemsView];
    }
}

-(void)updateItemsView{
    @autoreleasepool {
        NSInteger itemCount = 0;
        if([_dataSource respondsToSelector:@selector(itemCountInAlertView:)]){
            itemCount = [_dataSource itemCountInAlertView:self];
        }
        
        CGFloat height = 0;
        for (int i=0; i<itemCount; i++) {
            if([_dataSource respondsToSelector:@selector(itemViewAtIndex:inAlertView:)]){
                UIView *itemView = [_dataSource itemViewAtIndex:i inAlertView:self];
                if(itemView){
                    itemView.tag = kAlertView_ItemTag + i;
                    CGRect frame = itemView.frame;
                    frame.origin.y = height;
                    [itemView setFrame:frame];
                    [itemView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemViewDidSeleted:)]];
                    [_itemsView addSubview:itemView];
                    height += itemView.frame.size.height;
                }
            }
        }
        
        [_itemsView setFrame:CGRectMake(0, 0, kAlertView_Width, height)];
    }
}

- (void)itemViewDidSeleted:(UITapGestureRecognizer *)ges{
    UIView *view = ges.view;
    if(_delegate && [_delegate respondsToSelector:@selector(itemDidSeleted:atIndex:inAlertView:)]){
        [_delegate itemDidSeleted:view atIndex:view.tag - kAlertView_ItemTag inAlertView:self];
    }
}

- (void)updateSubviews{
    CGFloat height = [self heightWithTitleAndMessage];
    height += (height ? 10 : 0) + kAlertView_MarginY;
    _itemsView.frame = CGRectMake(0, height, kAlertView_Width, _itemsView.frame.size.height);
    height += _itemsView.frame.size.height + kAlertView_MarginY;
    _contentView.frame = CGRectMake(0, 0, kAlertView_Width, height);
    height += kAlertView_Space_Separation;
    _cancelView.frame = CGRectMake(0, height, kAlertView_Width, kAlertView_Height_Cancel);
    height += _cancelView.frame.size.height;
    
    _mainView.transform = CGAffineTransformIdentity;
    _mainView.frame = CGRectMake((self.frame.size.width - kAlertView_Width)/2, (self.frame.size.height - height)/2, kAlertView_Width, height);
    _mainView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:_mainView.bounds cornerRadius:_mainView.layer.cornerRadius].CGPath;
}

- (CGFloat)heightWithTitleAndMessage{
    CGFloat height = 0;
    if(_titleLayer){
        height += _titleLayer.frame.size.height;
        if(_messageLayer){
            height += kAlertView_Space_TitleMessage + _messageLayer.frame.size.height;
        }
    }else{
        if(_messageLayer){
            height += _messageLayer.frame.size.height;
        }
    }
    return height;
}

- (void)show{
    [LJAlertViewBackground showBackground];
    
    LJAlertViewController *viewController = [[LJAlertViewController alloc] init];
    viewController.alertView = self;
    
    if (!_alertWindow) {
        UIWindow *window = [[UIWindow alloc] initWithFrame:kBounds_Screen];
        window.opaque = NO;
        window.windowLevel = UIWindowLevelLJAlertView;
        window.rootViewController = viewController;
        _alertWindow = window;
    }
    [_alertWindow makeKeyAndVisible];
    
    [self transitionIn];
    
}


- (void)dismiss{
    [LJAlertViewBackground hideBackgroundAnimated:YES];
    
    [self transitionOut];
}

- (void)clearAll{
    [_mainView removeFromSuperview];
    [_contentView removeFromSuperview];
    _titleLayer = nil;
    _messageLayer = nil;
    [_cancelView removeFromSuperview];
    [_cancelBtns removeAllObjects];
    [_alertWindow removeFromSuperview];
    _alertWindow = nil;
}

- (void)transitionIn{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.values = @[@(0.01), @(1.2), @(0.9), @(1)];
    animation.keyTimes = @[@(0), @(0.4), @(0.6), @(1)];
    animation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    animation.duration = 0.5;
    [_mainView.layer addAnimation:animation forKey:@"bouce"];
}

- (void)transitionOut{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.values = @[@(1), @(1.2), @(0.01)];
    animation.keyTimes = @[@(0), @(0.4), @(1)];
    animation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    animation.duration = 0.35;
    animation.delegate = self;
    [_mainView.layer addAnimation:animation forKey:@"bounce"];
    _mainView.transform = CGAffineTransformMakeScale(0.01, 0.01);
}

#pragma mark - CAAnimation delegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self clearAll];
}

@end
