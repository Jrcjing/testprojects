//
//  CustomViewController.m
//  ContextTest
//
//  Created by Lisa on 15/4/15.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "CustomViewController.h"

@interface CustomViewController ()
{
    PieChart *_pie;
}
@end

@implementation CustomViewController


- (void)reloadPie{
    [_pie reloadData];
}

- (NSInteger)numberOfSlicesInPieChart:(PieChart *)pieChart{
    return rand()%10+1;
}

- (CGFloat)valueOfSlicesAtIndex:(NSInteger)index inPieChart:(PieChart *)pieChart{
    return rand()%60+20;
}

- (NSString *)descriptionOfSliecesAtIndex:(NSInteger)index inPieChart:(PieChart *)pieChart{
    return [NSString stringWithFormat:@"part_%ld",(long)index];
}

-(UIColor *)colorOfSlicesAtIndex:(NSInteger)index inPieChart:(PieChart *)pieChart{
    UIColor *color = [UIColor colorWithHue:(arc4random()%256)/256.0 saturation:(index%7+3)/10.0 brightness:91/100.0 alpha:1];
    //UIColor *color = [UIColor colorWithHue:((index/8)%20)/20.0+0.02 saturation:(index%8+3)/10.0 brightness:91/100.0 alpha:1];
    return color;
}

-(void)loadView{
    [super loadView];
    
    WS(weakSelf);
    
    _pie = [[PieChart alloc] initWithFrame:CGRectZero];
    _pie.backgroundColor = [UIColor lightGrayColor];
    _pie.dataSource = self;
    [self.view addSubview:_pie];
    [_pie mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.view);
    }];
    
    __weak PieChart *weakPie = _pie;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitle:@"Update" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(reloadPie) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakPie.mas_bottom).with.offset(-50);
        make.right.equalTo(weakPie.mas_right).with.offset(-10);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _pie.frame = self.view.bounds;
}


- (void)viewDidAppear:(BOOL)animated{
    [self performSelector:@selector(reloadPie) withObject:nil afterDelay:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
