//
//  AppDelegate.h
//  ContextTest
//
//  Created by Lisa on 15/4/7.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "ViewController.h"
#import "RootViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow           *window;
@property (nonatomic, retain) ViewController     *controller;
@property (nonatomic, weak  ) RootViewController *currentController;
@end

