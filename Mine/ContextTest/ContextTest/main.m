//
//  main.m
//  ContextTest
//
//  Created by Lisa on 15/4/7.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
