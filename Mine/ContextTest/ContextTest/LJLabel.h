//
//  LJLabel.h
//  ContextTest
//
//  Created by Lisa on 15/5/7.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LJTextAlignment){
    LJTextAlignmentNatural = 0,
    LJTextAlignmentLeft,
    LJTextAlignmentRight,
    LJTextAlignmentCenter,
    LJTextAlignmentJustified,
};

@interface LJLabel : UIView
@property (nonatomic,strong) UIFont *font;
@property (nonatomic,copy)   NSString *text;
@property (nonatomic,strong) UIColor *textColor;
@property (nonatomic,assign) LJTextAlignment alignment;
@property (nonatomic,assign) BOOL wrapped;
@end
