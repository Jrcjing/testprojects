//
//  RootViewController.m
//  ContextTest
//
//  Created by Lisa on 15/4/16.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()
@end

@implementation RootViewController

-(void)loadView{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED > 60100
    self.edgesForExtendedLayout = UIRectEdgeNone;
#endif
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if (Version_Device >= 7.0){
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
#endif

//    self.view.frame = kRect_RootView;
//    
//    if([self.parentViewController isKindOfClass:[UINavigationController class]]){
//        self.view.frame = kRect_RootViewNoNav;
//    }else{
//        self.view.frame = kRect_RootView;
//    }
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [TheAppDelegate setCurrentController:self];
}

@end
