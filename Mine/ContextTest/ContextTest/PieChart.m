//
//  PieChart.m
//  ContextTest
//
//  Created by Lisa on 15/4/9.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "PieChart.h"
#import <math.h>
#import "UITool.h"

#define kStartAngle M_PI_2*3

#define kInfoRadius _pieRadius * 3/5

#define kLayerFont [UIFont fontWithName:@"Arial"      size:_pieRadius/10]
#define kInfoFont [UIFont fontWithName:@"Arial" size: kInfoRadius/4]
#define kDescriptFont [UIFont fontWithName:@"Arial"   size:_pieRadius/15]

#define kMarginX 5
#define kMarginY 30
#define kPieDesSpace 20
#define kDescriptMargin (_pieView.frame.origin.x + 50)
#define kDescriptColorWidth _pieRadius/15
#define kDescriptColorTextSpace 3
#define kDescriptSpaceX 10
#define kDescriptSpaceY 5

@interface PieLayer : CAShapeLayer
@property (nonatomic,assign) CGFloat value;
@property (nonatomic,assign) CGFloat percent;
@property (nonatomic,assign) double startAngle;
@property (nonatomic,assign) double endAngle;

- (void)createAnimationArcPathForKey:(NSString *)key fromValue:(NSNumber *)from toValue:(NSNumber *)to Delegate:(id)delegate;
@end

@implementation PieLayer

- (void)createAnimationArcPathForKey:(NSString *)key fromValue:(NSNumber *)from toValue:(NSNumber *)to Delegate:(id)delegate{
    CABasicAnimation *arcAnimation = [CABasicAnimation animationWithKeyPath:key];
    NSNumber *currentAngle = [self valueForKey:key];
    if(!currentAngle){
        currentAngle = from;
    }
    [arcAnimation setFromValue:currentAngle];
    [arcAnimation setToValue:to];
    [arcAnimation setDelegate:delegate];
    [arcAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    [self addAnimation:arcAnimation forKey:key];
    [self setValue:to forKey:key];
}

@end

@interface PieChart()
{
    UIScrollView   *_scrollContentView;
    UIView         *_pieView;
    UIView         *_infoView;
    UIView         *_descriptionView;
    NSTimer        *_timer;
    NSMutableArray *_animationArr;
    
}
@property (nonatomic, assign) CGFloat pieRadius;
@property (nonatomic, assign) CGPoint pieCenter;
@property (nonatomic, assign) CGFloat textRadius;

@end

@implementation PieChart

static CGPathRef CGPathCreateArc(CGPoint center, CGFloat radius, CGFloat startAngle, CGFloat endAngle)
{
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, center.x, center.y);
    
    CGPathAddArc(path, NULL, center.x, center.y, radius, startAngle, endAngle, 0);
    CGPathCloseSubpath(path);
    
    return path;
}

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    _pieCenter = CGPointMake(_pieView.frame.size.width/2, _pieView.frame.size.height/2);
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        
        WS(weakSelf);
        
        _scrollContentView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollContentView.backgroundColor = [UIColor clearColor];
        [self addSubview:_scrollContentView];
        [_scrollContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(weakSelf);
        }];
        
        
        __weak UIScrollView *weakScroll = _scrollContentView;
        _pieView = [[UIView alloc] initWithFrame:self.bounds];
        _pieView.backgroundColor = [UIColor clearColor];
        [_scrollContentView addSubview:_pieView];
        [_pieView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(weakScroll.centerX);
            make.top.equalTo(weakScroll.top).with.offset(kMarginY);
        }];
        
        __weak UIView *weakPie = _pieView;
        _infoView = [[UIView alloc] initWithFrame:self.bounds];
        _infoView.backgroundColor = [UIColor whiteColor];
        [_scrollContentView addSubview:_infoView];
        [_infoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(weakPie.centerX);
            make.centerY.equalTo(weakPie.centerY);
        }];
        
        _descriptionView = [[UIView alloc] initWithFrame:self.bounds];
        _descriptionView.backgroundColor = [UIColor clearColor];
        [_scrollContentView addSubview:_descriptionView];
        [_descriptionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(weakScroll);
            make.top.equalTo(weakPie.bottom).with.offset(kPieDesSpace);
        }];
        
    }
    return self;
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    self.pieRadius = MIN(self.bounds.size.width/2, self.bounds.size.height/2) - kMarginX*2;
    _animationArr = [NSMutableArray array];
    
    self.textRadius = _pieRadius - (_pieRadius-kInfoRadius)/2;
    
    [_pieView updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(_pieRadius*2, _pieRadius*2));
    }];
    [_infoView updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kInfoRadius *2, kInfoRadius *2));
    }];
}

- (void)setPieRadius:(CGFloat)pieRadius{
    _pieRadius = pieRadius;
    [_pieView.layer setCornerRadius:_pieRadius];
    [_infoView.layer setCornerRadius:kInfoRadius];
}

#pragma mark ReloadData
- (void)reloadData{
    if(!_dataSource && !_timer) return;
    
    CALayer *parentLayer = [_pieView layer];
    NSArray *sliceLayerArr = [parentLayer sublayers];
    
    CALayer *desParentLayer = [_descriptionView layer];
    NSArray *descripLayerArr = [desParentLayer sublayers];
    
    double startAngle = 0.0;
    double endAngle = startAngle;
    
    NSInteger sliceNum = [_dataSource numberOfSlicesInPieChart:self];
    
    double sum = 0.0;
    NSMutableArray *valueArr = [NSMutableArray arrayWithCapacity:sliceNum];
    for (int i=0; i<sliceNum; i++) {
        double value = [_dataSource valueOfSlicesAtIndex:i inPieChart:self];
        [valueArr addObject:[NSNumber numberWithDouble:value]];
        sum += value;
    }
    
    [self updateInfoLableWithValue:sum];
    
    NSMutableArray *angleArr = [NSMutableArray arrayWithCapacity:sliceNum];
    for (int i=0; i<sliceNum; i++) {
        double angle = sum ? ([[valueArr objectAtIndex:i] doubleValue] / sum) :0.0;
        [angleArr addObject:[NSNumber numberWithDouble:angle * M_PI * 2]];
    }
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.5];
    
    __block NSMutableArray *layersToRemove = nil;
    [CATransaction setCompletionBlock:^{
        [layersToRemove enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [obj removeFromSuperlayer];
        }];
        [layersToRemove removeAllObjects];
    }];
    
    BOOL firstCreate = (![sliceLayerArr count]) && sliceNum;
    layersToRemove = [NSMutableArray arrayWithArray:sliceLayerArr];
    BOOL isEnd = [sliceLayerArr count] && (!sliceNum || sum < 0);
    NSInteger diff = sliceNum - [sliceLayerArr count];
    
    if(isEnd) {
        for(PieLayer *layer in _pieView.layer.sublayers){
            [self updateLableForLayer:layer value:0];
            [layer createAnimationArcPathForKey:@"startAngle"
                                      fromValue:[NSNumber numberWithDouble:kStartAngle]
                                        toValue:[NSNumber numberWithDouble:kStartAngle]
                                       Delegate:self];
            [layer createAnimationArcPathForKey:@"endAngle"
                                      fromValue:[NSNumber numberWithDouble:kStartAngle]
                                        toValue:[NSNumber numberWithDouble:kStartAngle]
                                       Delegate:self];
        }
        [CATransaction commit];
        return;
    }
    
    for (int i=0; i<sliceNum; i++) {
        PieLayer *layer;
        CALayer *desLayer;
        double angle = [[angleArr objectAtIndex:i] doubleValue];
        endAngle += angle;
        double realStartAngle = kStartAngle + startAngle;
        double realEndAngle = kStartAngle + endAngle;
        CGFloat value = (CGFloat)[[valueArr objectAtIndex:i] doubleValue];
        
        if(i >= [sliceLayerArr count]){
            layer = [self createPieLayer];
            if(firstCreate){
                realStartAngle = realEndAngle = kStartAngle;
            }
            [parentLayer addSublayer:layer];
            
            desLayer = [self createDesLayerWithIndex:i];
            [desParentLayer addSublayer:desLayer];
            diff--;
        }else{
            PieLayer *currentLayer = (PieLayer *)[sliceLayerArr objectAtIndex:i];
            CALayer *currentDesLayer = [descripLayerArr objectAtIndex:i];
            
            if(!diff || currentLayer.value == value){
                layer = currentLayer;
                [layersToRemove removeObject:layer];
                
                desLayer = currentDesLayer;
            }else if (diff > 0){
                layer = [self createPieLayer];
                [parentLayer insertSublayer:layer atIndex:i];
                
                desLayer = [self createDesLayerWithIndex:i];
                [desParentLayer insertSublayer:desLayer atIndex:i];
                diff--;
            }else if (diff < 0){
                for (int j=diff; j<0; j++) {
                    PieLayer *layerToRemove = (PieLayer *)[sliceLayerArr objectAtIndex:sliceLayerArr.count-1];
                    [layerToRemove removeFromSuperlayer];
                    
                    CALayer *desLayerToRemove = [descripLayerArr objectAtIndex:descripLayerArr.count-1];
                    [desLayerToRemove removeFromSuperlayer];
                    diff++;
                }
                if(currentLayer.value == value || !diff){
                    layer = currentLayer;
                    [layersToRemove removeObject:layer];
                    
                    desLayer = currentDesLayer;
                }
            }
        }
        
        UIColor *color = [_dataSource colorOfSlicesAtIndex:i inPieChart:self];
        layer.fillColor = color.CGColor;
        [self updateLableForLayer:layer value:value];
        
        NSString *descriptionStr = [_dataSource descriptionOfSliecesAtIndex:i inPieChart:self];
        [self updateDesLayer:desLayer withColor:color andDescription:descriptionStr inIndex:i];
        
        if(i == sliceNum-1){
            [_descriptionView setFrame:CGRectMake(0, _pieView.frame.origin.y + _pieView.frame.size.height + kPieDesSpace, self.frame.size.width, desLayer.frame.origin.y + desLayer.frame.size.height)];
            [_scrollContentView setContentSize:CGSizeMake(_scrollContentView.frame.size.width, MAX(_scrollContentView.frame.size.height, _descriptionView.frame.origin.y + _descriptionView.frame.size.height + 10))];
            
        }
        
        
        [layer createAnimationArcPathForKey:@"startAngle"
                                  fromValue:[NSNumber numberWithDouble:realStartAngle]
                                    toValue:[NSNumber numberWithDouble:startAngle+kStartAngle]
                                   Delegate:self];
        [layer createAnimationArcPathForKey:@"endAngle"
                                  fromValue:[NSNumber numberWithDouble:realEndAngle]
                                    toValue:[NSNumber numberWithDouble:endAngle+kStartAngle]
                                   Delegate:self];
        
        startAngle = endAngle;
    }
    
    [CATransaction setDisableActions:YES];
    for(PieLayer *layer in layersToRemove){
        layer.fillColor = self.backgroundColor.CGColor;
        layer.delegate = nil;
        CATextLayer *textLayer = [[layer sublayers] objectAtIndex:0];
        [textLayer setHidden:YES];
    }
    [CATransaction setDisableActions:NO];
    [CATransaction commit];
}

- (void)updateInfoLableWithValue:(NSInteger)value{
    CALayer *parentLayer = [_infoView layer];
    CATextLayer *textLayer;
    if(![[parentLayer sublayers] count]){
        textLayer = [UITool createTextLayerWithFont:kInfoFont];
        [textLayer setAlignmentMode:kCAAlignmentCenter];
        [textLayer setWrapped:YES];
        [parentLayer addSublayer:textLayer];
    }else{
        textLayer = [[parentLayer sublayers] objectAtIndex:0];
    }
    
    NSString *text = [NSString stringWithFormat:@"%d\nLeads",value<0?0:value];
    NSMutableAttributedString *attributedStr = [UITool attributedStringWithText:text textColor:[UIColor blackColor] font:kInfoFont];
    [textLayer setString:attributedStr];
    
    CGSize size = [UITool sizeWithAttributeString:attributedStr font:kInfoFont boundingRectWithSize:parentLayer.frame.size];
    [textLayer setFrame:CGRectMake(0, 0, size.width, size.height)];
    [textLayer setPosition:CGPointMake(parentLayer.frame.size.width/2, parentLayer.frame.size.height/2)];
}

- (void)updateLableForLayer:(PieLayer *)pieLayer value:(CGFloat)value{
    CATextLayer *textLayer = [[pieLayer sublayers] objectAtIndex:0];
    NSString *text = [NSString stringWithFormat:@"%.0f",value];
    CGSize size = [UITool sizeWithOneLineText:text font:kLayerFont];
    [CATransaction setDisableActions:YES];

    if(value > 0){
        [textLayer setString:text];
        [textLayer setBounds:CGRectMake(0, 0, size.width, size.height)];
    }

    [CATransaction setDisableActions:NO];
}

- (void)updateDesLayer:(CALayer *)layer withColor:(UIColor *)color andDescription:(NSString *)description inIndex:(NSInteger)index{
    CALayer *colorLayer = [[layer sublayers] objectAtIndex:0];
    [colorLayer setBackgroundColor:[color CGColor]];
    
    CATextLayer *textLayer = [[layer sublayers] objectAtIndex:1];
    NSMutableAttributedString *attributedStr = [UITool attributedStringWithText:description textColor:[UIColor blackColor] font:kDescriptFont];
    [textLayer setString:attributedStr];
    
    CGFloat width = layer.frame.size.width;
    CGFloat height = layer.frame.size.height;
    CGPoint position = [self positionForDesLayerInIndex:index withWidth:width andHeight:height];
    [CATransaction setDisableActions:YES];
    [layer setPosition:position];
    [CATransaction setDisableActions:NO];
}


- (PieLayer *)createPieLayer{
    PieLayer *layer = [PieLayer layer];
    [layer setStrokeColor:NULL];
    
    CATextLayer *textLayer = [UITool createTextLayerWithFont:kLayerFont];
    [textLayer setAlignmentMode:kCAAlignmentCenter];
    
    CGSize size = [UITool sizeWithOneLineText:@"0" font:kLayerFont];
    [CATransaction setDisableActions:YES];
    [textLayer setFrame:CGRectMake(0, 0, size.width, size.height)];
    [textLayer setPosition:CGPointMake(_pieCenter.x + (_textRadius * cos(0)), _pieCenter.y + (_textRadius * sin(0)))];
    [CATransaction setDisableActions:NO];
    [layer addSublayer:textLayer];
    return layer;
}

- (CALayer *)createDesLayerWithIndex:(NSInteger)index{
    CALayer *layer = [CALayer layer];
    CGFloat layerWidth = (_descriptionView.frame.size.width - kDescriptMargin*2 - kDescriptSpaceX)/2;
    
    CALayer *colorLayer = [CALayer layer];
    [layer addSublayer:colorLayer];
    
    CATextLayer *textLayer = [UITool createTextLayerWithFont:kDescriptFont];
    
    CGFloat width = layerWidth - kDescriptColorWidth - kDescriptColorTextSpace;
    CGSize size = [UITool sizeWithOneLineText:@"0" font:kDescriptFont];
    size.width = width;
    CGFloat height = MAX(kDescriptColorWidth, size.height);
    [layer addSublayer:textLayer];
    
    CGPoint position = [self positionForDesLayerInIndex:index withWidth:layerWidth andHeight:height];
    
    [CATransaction setDisableActions:YES];
    [layer setFrame:CGRectMake(0, 0, layerWidth, height)];
    [layer setPosition:position];
    [colorLayer setFrame:CGRectMake(0, 0, kDescriptColorWidth, kDescriptColorWidth)];
    [colorLayer setPosition:CGPointMake(kDescriptColorWidth/2, height/2)];
    [textLayer setFrame:CGRectMake(0, 0, width, size.height)];
    [textLayer setPosition:CGPointMake(kDescriptColorWidth + kDescriptColorTextSpace + width/2, height/2)];
    [textLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [CATransaction setDisableActions:NO];
    
    return layer;
}

- (CGPoint)positionForDesLayerInIndex:(NSInteger)index withWidth:(CGFloat)width andHeight:(CGFloat)height{
    CGFloat positionX = (width + kDescriptSpaceX) * ((index+2)%2) + (width/2) + kDescriptMargin;
    CGFloat positionY = (height + kDescriptSpaceY) * (index/2) +(height/2);
    return CGPointMake(positionX, positionY);
}

- (void)updateTimer{
    CALayer *parentLayer = [_pieView layer];
    NSArray *layers = [parentLayer sublayers];
    
    [layers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        PieLayer *layer = obj;
        CGFloat startAngle = [[[layer presentationLayer] valueForKey:@"startAngle"] doubleValue];
        CGFloat endAngle = [[[layer presentationLayer] valueForKey:@"endAngle"] doubleValue];
        
        if(!startAngle && !endAngle) return;
        CGPathRef path = CGPathCreateArc(_pieCenter, _pieRadius, startAngle, endAngle);
        [layer setPath:path];
        CFRelease(path);
        
        CALayer *textLayer = [[obj sublayers] objectAtIndex:0];
        CGFloat angle = (endAngle + startAngle)/2;
        [CATransaction setDisableActions:YES];
        [textLayer setPosition:CGPointMake(_pieCenter.x + (_textRadius * cos(angle)), _pieCenter.y + (_textRadius * sin(angle)))];
        [CATransaction setDisableActions:NO];
    }];
}

- (void)animationDidStart:(CAAnimation *)anim{
    if(!_timer){
        static float timeInterval = 1.0/60.0;
        _timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    }

    [_animationArr addObject:anim];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    [_animationArr removeObject:anim];
    
    if(!_animationArr.count){
        [_timer invalidate];
        _timer = nil;
    }
}


@end
