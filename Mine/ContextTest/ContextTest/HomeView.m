//
//  HomeView.m
//  ContextTest
//
//  Created by Lisa on 15/4/15.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "HomeView.h"
#import "CustomViewController.h"
#import "LJAlertView.h"
#import "UITool.h"
#import "LJLabel.h"
#import "LJProgressView.h"
#import "LJTextField.h"
#import "IQKeyboardManager.h"
#import "MasonryDemoViewController.h"

@interface HomeView()<LJAlertViewDataSource,LJAlertViewDelegate,UITextFieldDelegate>
{
    NSTimer *_progressTimer;
}
@end


@implementation HomeView

-(void)dealloc{
    [_progressTimer invalidate];
    _progressTimer = nil;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn1 setBackgroundColor:[UIColor clearColor]];
        [btn1 setTitle:@"PieChart" forState:UIControlStateNormal];
        [btn1 setFrame:CGRectMake((frame.size.width-100)/2, 100, 100, 50)];
        [btn1 addTarget:self action:@selector(pieChart) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn1];
        
        UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn2 setBackgroundColor:[UIColor clearColor]];
        [btn2 setTitle:@"Alert" forState:UIControlStateNormal];
        [btn2 setFrame:CGRectMake((frame.size.width-100)/2, 200, 100, 50)];
        [btn2 addTarget:self action:@selector(alert) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn2];
        
        UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn3 setBackgroundColor:[UIColor clearColor]];
        [btn3 setTitle:@"Progress" forState:UIControlStateNormal];
        [btn3 setFrame:CGRectMake((frame.size.width-100)/2, 300, 100, 50)];
        [btn3 addTarget:self action:@selector(progress) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn3];
        
        UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn4 setBackgroundColor:[UIColor clearColor]];
        [btn4 setTitle:@"TextField" forState:UIControlStateNormal];
        [btn4 setFrame:CGRectMake((frame.size.width-100)/2, 400, 100, 50)];
        [btn4 addTarget:self action:@selector(textfield) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn4];
        
        UIButton *btn5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn5 setBackgroundColor:[UIColor clearColor]];
        [btn5 setTitle:@"MasonryDemo" forState:UIControlStateNormal];
        [btn5 setFrame:CGRectMake((frame.size.width-100)/2, 500, 100, 50)];
        [btn5 addTarget:self action:@selector(masonry) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn5];
    }
    return self;
}

- (void)pieChart{
    CustomViewController *controller = [[CustomViewController alloc] init];
//    UIViewController *currentControl = [(AppDelegate *)[UIApplication sharedApplication].delegate currentController];
    [CurrentController.navigationController pushViewController:controller animated:YES];
}

- (void)alert{
    LJAlertView *alert = [[LJAlertView alloc] initWithTitle:@"Title" message:@"message:awoigaowienfoawinefoiawnefoaiwnefoaiwenfoiawnefuyiyuciyucitciytciyc" cancelBtns:[NSArray arrayWithObjects:@"Ok", nil]];
    alert.tag = 100;
    alert.dataSource = self;
    alert.delegate = self;
    [alert show];
}

static CGFloat progress = 0;

- (void)progress{
    LJAlertView *alert = [[LJAlertView alloc] initWithTitle:nil message:@"Downloading data..." cancelBtns:[NSArray arrayWithObjects:@"Cancel Download", nil]];
    alert.tag = 200;
    alert.dataSource = self;
    [alert show];
    
    
    progress = 0;
    _progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgress) userInfo:nil repeats:YES];
}

- (void)updateProgress{
    if(progress>=1 && _progressTimer){
        [self clearTimer];
        return;
    }
    
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;

    LJAlertView *alertView = (LJAlertView *)[keywindow viewWithTag:200];
    if(!alertView){
        [self clearTimer];
        return;
    }
    
    LJProgressView *progressView = (LJProgressView *)[alertView viewWithTag:12345];
    if(!progressView){
        [self clearTimer];
        return;
    }
    
    progress += 0.05;
    progressView.progress = progress;
}

- (void)clearTimer{
    [_progressTimer invalidate];
    _progressTimer = nil;
}


-(void)textfield{
    LJAlertView *alert = [[LJAlertView alloc] initWithTitle:@"Download Lead from your Exsiting customer" message:@"afadvasgagasfasfasdfasdfasdf" cancelBtns:[NSArray arrayWithObjects:@"Download",@"Cancel", nil]];
    alert.tag = 300;
    alert.dataSource = self;
    [alert show];
}


#pragma mark LJAlertView DataSource
- (NSInteger)itemCountInAlertView:(LJAlertView *)alertView{
    NSInteger num = 0;
    switch (alertView.tag) {
        case 100:
            num = 5;
            break;
        case 200:
            num = 1;
            break;
        case 300:
            num = 4;
            break;
        default:
            break;
    }
    return num;
}

- (UIView *)itemViewAtIndex:(NSInteger)index inAlertView:(LJAlertView *)alertView{
    UIView *view = nil;
    switch (alertView.tag) {
        case 100:
        {
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kAlertView_Width, 40)];
            view.backgroundColor = [UIColor clearColor];
            NSString *text = [NSString stringWithFormat:@"item_hi奥数的分红啊温柔结构发觉减肥哈我发哈维京哈而非_ %ld",(long)index];
            LJLabel *label = [[LJLabel alloc] initWithFrame:view.bounds];
            label.text = text;
            label.font = [UIFont systemFontOfSize:15];
            label.textColor = [UIColor grayColor];
            label.alignment = LJTextAlignmentCenter;
            label.wrapped = YES;
            [view addSubview:label];
        }
            break;
        case 200:
        {
            CGRect frame = CGRectMake(0, 0, kAlertView_Width, 60);
            view = [[UIView alloc] initWithFrame:frame];
            view.backgroundColor = [UIColor clearColor];
            
            frame.origin.x = 50;
            frame.size.width -= 100;
            __weak LJAlertView *weakAlert = alertView;
            LJProgressView *progress = [[LJProgressView alloc] initWithFrame:frame progressEnd:^{
                NSLog(@"下载完成！");
                [weakAlert dismiss];
            }];
            progress.tag = 12345;
            [view addSubview:progress];
        }
            break;
        case 300:
        {
            CGRect frame = CGRectMake(0, 0, kAlertView_Width, 50);
            view = [[UIView alloc] initWithFrame:frame];
            view.backgroundColor = [UIColor clearColor];
            
            frame.origin.x = 50;
            frame.origin.y = 10;
            frame.size.width -= 100;
            frame.size.height -= 10;
            LJTextField *field = [[LJTextField alloc] initWithFrame:frame];
            field.delegate = self;
            field.returnKeyType = UIReturnKeyNext;
            [view addSubview:field];
        }
            break;
        default:
            break;
    }
    
    return view;
}

#pragma mark LJAlertView Delegate
- (void)itemDidSeleted:(UIView *)view atIndex:(NSInteger)index inAlertView:(LJAlertView *)alertView{
    NSLog(@"item_ %ld",(long)index);
    [alertView dismiss];
}

- (void)cancelBtnDidSelected:(UIButton *)cancelBtn atIndex:(NSInteger)index inAlertView:(LJAlertView *)alertView{
    NSLog(@"cancelBtn_%ld",(long)index);
    [alertView dismiss];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [[IQKeyboardManager sharedManager] goNextNil];
    return NO;
}


- (void)masonry{
    MasonryDemoViewController *controller = [[MasonryDemoViewController alloc] init];
    [[CurrentController navigationController] pushViewController:controller animated:YES];
}
@end
