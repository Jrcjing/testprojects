//
//  LJLabel.m
//  ContextTest
//
//  Created by Lisa on 15/5/7.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "LJLabel.h"
#import "UITool.h"

@interface LJLabel()
@property (nonatomic,retain) CATextLayer *textLayer;
@end

@implementation LJLabel

-(id)init{
    self = [super init];
    if(self){
        self.textLayer = [UITool createTextLayer];
        [self.layer addSublayer:_textLayer];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.textLayer = [UITool createTextLayer];
        [self.layer addSublayer:_textLayer];
    }
    return self;
}

- (void)setFont:(UIFont *)font{
    if(_font != font){
        _font = font;
        
        CGFontRef fontRef = CGFontCreateWithFontName((__bridge CFStringRef)font.fontName);
        [_textLayer setFont:fontRef];
        [_textLayer setFontSize:font.pointSize];
        
        [self setNeedsLayout];
    }
}

- (void)setText:(NSString *)text{
    if(_text != text){
        _text = text;
        [_textLayer setString:_text];
        [self setNeedsLayout];
    }
}

- (void)setTextColor:(UIColor *)textColor{
    if(_textColor != textColor){
        _textColor = textColor;
        [_textLayer setForegroundColor:[textColor CGColor]];
        [self setNeedsLayout];
    }
}

- (void)setAlignment:(LJTextAlignment)alignment{
    if(_alignment != alignment){
        _alignment = alignment;
        
        NSString *textLayerAlignment = kCAAlignmentNatural;
        switch (alignment) {
            case LJTextAlignmentNatural:
                textLayerAlignment = kCAAlignmentNatural;
                break;
            case LJTextAlignmentLeft:
                textLayerAlignment = kCAAlignmentLeft;
                break;
            case LJTextAlignmentRight:
                textLayerAlignment = kCAAlignmentRight;
                break;
            case LJTextAlignmentCenter:
                textLayerAlignment = kCAAlignmentCenter;
                break;
            case LJTextAlignmentJustified:
                textLayerAlignment = kCAAlignmentJustified;
                break;
            default:
                break;
        }
        [_textLayer setAlignmentMode:textLayerAlignment];
        [self setNeedsLayout];
    }
    
}

- (void)setWrapped:(BOOL)wrapped{
    if(_wrapped != wrapped){
        _wrapped = wrapped;
        [_textLayer setWrapped:_wrapped];
        [self setNeedsLayout];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    if(!_text.length) return;
    NSMutableAttributedString *attStr = [UITool attributedStringWithText:_text textColor:_textColor font:_font];
    [_textLayer setString:attStr];
    CGSize size = CGSizeZero;
    if(_wrapped){
        size = [UITool sizeWithAttributeString:attStr font:_font boundingRectWithSize:CGSizeMake(self.bounds.size.width, CGFLOAT_MAX)];
    }else{
        size = [UITool sizeWithOneLineText:_text font:_font];
    }
    [_textLayer setFrame:CGRectMake(0, 0, size.width, size.height)];
    
    CGRect frame = self.frame;
    frame.size.width = size.width;
    frame.size.height = size.height;
    self.frame = frame;
}

@end
