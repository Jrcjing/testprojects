//
//  ProgressView.h
//  ContextTest
//
//  Created by Lisa on 15/4/20.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LJProgressView : UIView
{
    void (^LJProgressEndBlock)();
}
@property (nonatomic,assign) CGFloat progress;
-(id)initWithFrame:(CGRect)frame progressEnd:(void(^)())progressDidEnd;
@end
