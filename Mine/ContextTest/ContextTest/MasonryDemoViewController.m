//
//  MasonryViewController.m
//  ContextTest
//
//  Created by Lisa on 15/5/13.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "MasonryDemoViewController.h"
#import "MasonryDemoView.h"

@interface MasonryDemoViewController ()
{
    MasonryDemoView *_theView;
}
@end

@implementation MasonryDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    WS(weakSelf);
    
    //_theView = [[MasonryDemoView alloc] initWithFrame:self.view.bounds];
    _theView = [MasonryDemoView new];
    [_theView showPlaceHolder];
    [self.view addSubview:_theView];
    [_theView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.view);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
