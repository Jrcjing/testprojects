//
//  UITool.h
//  ContextTest
//
//  Created by Lisa on 15/4/20.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>

@interface UITool : NSObject

+ (CATextLayer *)createTextLayer;
+ (CATextLayer *)createTextLayerWithFont:(UIFont *)font;
+ (CATextLayer *)createTextLayerWithText:(NSString *)text Font:(UIFont *)font textColor:(UIColor *)color alignment:(NSString *)alignment inSize:(CGSize)boundWithSize;

+ (NSMutableAttributedString *)attributedStringWithText:(NSString *)text textColor:(UIColor *)color font:(UIFont *)font;
+ (CGSize)sizeWithAttributeString:(NSAttributedString *)attStr font:(UIFont *)font boundingRectWithSize:(CGSize)boundWithSize;
+ (CGSize)sizeWithOneLineText:(NSString *)text font:(UIFont *)font;
@end
