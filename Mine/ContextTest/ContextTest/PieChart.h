//
//  PieChart.h
//  ContextTest
//
//  Created by Lisa on 15/4/9.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

@class PieChart;

@protocol PieChartDataSource <NSObject>
@required
- (NSInteger)numberOfSlicesInPieChart:(PieChart *)pieChart;
- (CGFloat)valueOfSlicesAtIndex:(NSInteger)index inPieChart:(PieChart *)pieChart;
- (NSString *)descriptionOfSliecesAtIndex:(NSInteger)index inPieChart:(PieChart *)pieChart;
- (UIColor *)colorOfSlicesAtIndex:(NSInteger)index inPieChart:(PieChart *)pieChart;
@end

@interface PieChart : UIView
@property(nonatomic, weak) id<PieChartDataSource> dataSource;
- (void)reloadData;
@end
