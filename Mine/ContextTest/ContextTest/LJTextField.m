//
//  LJTextView.m
//  ContextTest
//
//  Created by Lisa on 15/4/20.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "LJTextField.h"
#import "UITool.h"

@implementation LJTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.placeholder = @"PlaceHolder";
        self.textAlignment = NSTextAlignmentCenter;
        
        self.layer.borderWidth = 1.0;
        self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
    return self;
    
}

- (CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 20, 10);
}

- (CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 20, 10);
}

@end
