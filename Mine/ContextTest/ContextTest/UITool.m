//
//  UITool.m
//  ContextTest
//
//  Created by Lisa on 15/4/20.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "UITool.h"

@implementation UITool

+ (CATextLayer *)createTextLayer{
    CATextLayer *layer = [CATextLayer layer];
    [layer setAlignmentMode:kCAAlignmentLeft];
    [layer setAnchorPoint:CGPointMake(0.5, 0.5)];
    [layer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [layer setContentsScale:[[UIScreen mainScreen] scale]];
    return layer;
}

+ (CATextLayer *)createTextLayerWithFont:(UIFont *)font{
    CATextLayer *layer = [UITool createTextLayer];
    CGFontRef fontRef = CGFontCreateWithFontName((__bridge CFStringRef)font.fontName);
    [layer setFont:fontRef];
    [layer setFontSize:font.pointSize];
    return layer;
}

+ (CATextLayer *)createTextLayerWithText:(NSString *)text Font:(UIFont *)font textColor:(UIColor *)color alignment:(NSString *)alignment inSize:(CGSize)boundWithSize{
    CATextLayer *layer = [UITool createTextLayerWithFont:font];
    [layer setAlignmentMode:alignment];
    [layer setWrapped:YES];
    
    NSMutableAttributedString *attStr = [self attributedStringWithText:text textColor:color font:font];
    CGSize size = [self sizeWithAttributeString:attStr font:font boundingRectWithSize:boundWithSize];
    [layer setFrame:CGRectMake(0, 0, boundWithSize.width, size.height)];
    [layer setString:attStr];
    
    return layer;
}


+ (NSMutableAttributedString *)attributedStringWithText:(NSString *)text textColor:(UIColor *)color font:(UIFont *)font{
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedStr addAttribute:(NSString *)kCTFontAttributeName value:(id)CFBridgingRelease(CTFontCreateWithName((__bridge CFStringRef)font.fontName,font.pointSize,NULL)) range:range];
    [attributedStr addAttribute:(NSString *)kCTForegroundColorAttributeName value:color range:range];
    return attributedStr;
}

+ (CGSize)sizeWithAttributeString:(NSAttributedString *)attStr font:(UIFont *)font boundingRectWithSize:(CGSize)boundWithSize{
    NSString *text = attStr.string;
    CGSize size = CGSizeZero;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000 && __IPHONE_OS_VERSION_MAX_ALLOWED <70000
    size = [text sizeWithFont:font constrainedToSize:boundWithSize];
#else
    NSRange range = NSMakeRange(0, text.length);
    NSDictionary *attDic = [attStr attributesAtIndex:0 effectiveRange:&range];
    size = [text boundingRectWithSize:boundWithSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attDic context:nil].size;
#endif
    return size;
}

+ (CGSize)sizeWithOneLineText:(NSString *)text font:(UIFont *)font{
    CGSize size = CGSizeZero;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000 && __IPHONE_OS_VERSION_MAX_ALLOWED <70000
    size = [text sizeWithFont:font];
#else
    size = [text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil]];
#endif
    return size;
}

@end
