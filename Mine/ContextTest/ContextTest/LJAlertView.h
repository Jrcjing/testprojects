//
//  AlertView.h
//  ContextTest
//
//  Created by Lisa on 15/4/15.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//


#define kAlertView_Width 400
#define kAlertView_Space_Separation 10
#define kAlertView_Height_Cancel 40
#define kAlertView_Space_Btns 5
#define kAlertView_MarginX 30
#define kAlertView_MarginY 20
#define kAlertView_Space_TitleMessage 10
#define kAlertView_Font_Title [UIFont boldSystemFontOfSize:20]
#define kAlertView_Font_Merssage [UIFont fontWithName:@"Arial" size:18]

#define kAlertView_CancelTag 99999
#define kAlertView_ItemTag 999

@class LJAlertView;
@protocol LJAlertViewDataSource <NSObject>
- (NSInteger)itemCountInAlertView:(LJAlertView *)alertView;
- (UIView *)itemViewAtIndex:(NSInteger)index inAlertView:(LJAlertView *)alertView;
@end

@protocol LJAlertViewDelegate <NSObject>
- (void)cancelBtnDidSelected:(UIButton *)cancelBtn atIndex:(NSInteger)index inAlertView:(LJAlertView *)alertView;
- (void)itemDidSeleted:(UIView *)view atIndex:(NSInteger)index inAlertView:(LJAlertView *)alertView;
@end



@interface LJAlertView : UIView
@property (nonatomic,weak) id <LJAlertViewDataSource> dataSource;
@property (nonatomic,weak) id <LJAlertViewDelegate> delegate;

- (id)initWithTitle:(NSString *)title message:(NSString *)message cancelBtns:(NSArray *)cancelBtns;
- (void)setUp;
- (void)show;
- (void)dismiss;
- (void)reloadData;
@end
