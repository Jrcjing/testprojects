//
//  CustomView.m
//  ContextTest
//
//  Created by Lisa on 15/4/7.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "CustomView.h"
#import "PieChart.h"

@interface CustomView()

@end

@implementation CustomView

- (id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.clipsToBounds = NO;
        
    }
    return self;
}


@end
