//
//  ProgressView.m
//  ContextTest
//
//  Created by Lisa on 15/4/20.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#import "LJProgressView.h"
#import "UITool.h"

@interface LJProgressView()
{
    CALayer     *_progressBackgroundLayer;
    CALayer     *_progressLayer;
    CATextLayer *_progressTextLayer;
}
@end

@implementation LJProgressView

-(id)initWithFrame:(CGRect)frame progressEnd:(void (^)())progressDidEnd{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        
        _progressBackgroundLayer = [CALayer layer];
        [_progressBackgroundLayer setBackgroundColor:[[UIColor lightGrayColor] CGColor]];
        [_progressBackgroundLayer setFrame:CGRectMake(0, 0, frame.size.width, 30)];
        [self.layer addSublayer:_progressBackgroundLayer];
        
        _progressLayer = [CALayer layer];
        [_progressLayer setBackgroundColor:[[UIColor redColor] CGColor]];
        [_progressLayer setFrame:CGRectMake(0, 0, 0 , _progressBackgroundLayer.frame.size.height)];
        [_progressBackgroundLayer addSublayer:_progressLayer];
        
        LJProgressEndBlock = [progressDidEnd copy];
    }
    return self;
}

-(void)setProgress:(CGFloat)progress{
    progress = (progress<0) ? 0 : ((progress>1) ? 1: progress);
    if(_progress == progress) return;
    _progress = progress;
    
    if(_progress == 1){
        [CATransaction setCompletionBlock:^{
            if(LJProgressEndBlock){
                LJProgressEndBlock();
            }
        }];
    }
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.3];
    [CATransaction lock];
    CGRect frame = _progressLayer.frame;
    frame.size.width = _progressBackgroundLayer.frame.size.width * progress;
    _progressLayer.frame = frame;
    [CATransaction commit];
    
    [self updateText];
}

-(void)updateText{
    UIFont *font = [UIFont systemFontOfSize:15];
    UIColor *color = [UIColor grayColor];
    NSString *str = [NSString stringWithFormat:@"%.f%%",_progress*100];
    if(!_progressTextLayer){
        _progressTextLayer = [UITool createTextLayerWithText:str Font:font textColor:color alignment:kCAAlignmentCenter inSize:CGSizeMake(self.frame.size.width, MAXFLOAT)];
        CGRect frame = _progressTextLayer.frame;
        frame.origin.y = _progressBackgroundLayer.frame.origin.y + _progressBackgroundLayer.frame.size.height + 10;
        [_progressTextLayer setFrame:frame];
        [self.layer addSublayer:_progressTextLayer];
        return;
    }
    
    NSMutableAttributedString *attStr = [UITool attributedStringWithText:str textColor:color font:font];
    [_progressTextLayer setString:attStr];
}

@end
