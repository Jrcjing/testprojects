//
//  UIDefine.h
//  ContextTest
//
//  Created by Lisa on 15/4/16.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

#ifndef ContextTest_UIDefine_h
#define ContextTest_UIDefine_h

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self

#define TheAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]
#define CurrentController [TheAppDelegate currentController]
#define CurrentController_isNav [CurrentController.parentViewController isKindOfClass:[UINavigationController class]]

#define Version_MaxAllowed __IPHONE_OS_VERSION_MAX_ALLOWED
#define Version_Device [[[UIDevice currentDevice] systemVersion] floatValue]

#define isIOS6 (Version_MaxAllowed >= 60000 && Version_MaxAllowed < 70000)
#define isIOS6Above (Version_MaxAllowed >= 60000)
#define isIOS7 (Version_MaxAllowed >= 70000 && Version_MaxAllowed < 80000)
#define isIOS7Above (Version_MaxAllowed >= 70000)
#define isIOS8 (Version_MaxAllowed >= 80000)

#define isIOS6_Device (Version_Device>= 6.0 && Version_Device < 7.0)
#define isIOS6Above_Device (Version_Device >= 6.0)
#define isIOS7_Device (Version_Device >=7.0 && Version_Device < 8.0)
#define isIOS7Above_Device (Version_Device >=7.0)
#define isIOS8_Device (Version_Device >= 8.0)

#define kHeight_Statuebar 20
#define kHeight_Tabbar 49
#define kHeight_Navbar 44

#define kBounds_Screen [UIScreen mainScreen].bounds
#define kWidth_Screen ((isIOS6 || isIOS7) ? kBounds_Screen.size.height : kBounds_Screen.size.width)
#define kHeight_Screen ((isIOS6 ||isIOS7) ? (kBounds_Screen.size.width - kHeight_Statuebar) : (isIOS7Above ? (kBounds_Screen.size.height - kHeight_Statuebar) : kBounds_Screen.size.height))
#define kBounds_Screen_Fixed CGRectMake(kBounds_Screen.origin.x, kBounds_Screen.origin.y, kWidth_Screen, kHeight_Screen)

#define kWidth_RootView kWidth_Screen
#define kHeight_RootView kHeight_Screen
#define kHeight_RootViewNoNav (kHeight_RootView - kHeight_Navbar)
#define kHeight_RootViewNoTab (kHeight_RootViewNoNav - kHeight_Tabbar)

#define kRect_RootView CGRectMake(0,0,kWidth_RootView,kHeight_RootView)
#define kRect_RootViewNoNav CGRectMake(0,0,kWidth_RootView,kHeight_RootViewNoNav)
#define kRect_RootViewNoTab CGRectMake(0,0,kWidth_RootView,kHeight_RootViewNoTab)

#endif
