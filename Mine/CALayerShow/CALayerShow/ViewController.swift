//
//  ViewController.swift
//  CALayerShow
//
//  Created by Lisa on 15/3/27.
//  Copyright (c) 2015年 Lisa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewForLayer: UIView!
    
    var l: CALayer {
    return viewForLayer.layer
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setUpLayer()
    }
    
    func setUpLayer() {
//        l.backgroundColor = UIColor.grayColor().CGColor
//        l.borderWidth = 100.0
//        l.borderColor = UIColor.lightGrayColor().CGColor
//        l.shadowOpacity = 0.7
//        l.shadowRadius = 10.0
//        l.contents = UIImage(named: "star")?.CGImage
//        l.contentsGravity = kCAGravityCenter
        
        var layer = CALayer();
        layer.frame = viewForLayer.bounds
        layer.contents = UIImage(named: "star")?.CGImage
        layer.contentsGravity = kCAGravityCenter
        layer.magnificationFilter = kCAFilterLinear
        layer.geometryFlipped = false
        layer.backgroundColor = UIColor(red: 11/255.0, green: 86/255.0, blue: 14/255.0, alpha: 1.0).CGColor
        layer.opacity = 1.0
        layer.hidden = false
        layer.masksToBounds = false
        layer.cornerRadius = layer.frame.size.width/2
        layer.borderWidth = 12.0
        layer.borderColor = UIColor.whiteColor().CGColor
        layer.shadowOpacity = 0.75
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowRadius = 3.0
        viewForLayer.layer.addSublayer(layer)
    }

//    @IBAction func tapGestureRecognizer(sender: UITapGestureRecognizer) {
//        l.shadowOpacity = l.shadowOpacity == 0.7 ? 0.0 : 0.7
//    }
//    
//    
//    @IBAction func pinchGestureRecognizer(sender: UIPinchGestureRecognizer) {
//        let offset : CGFloat = sender.scale < 1 ? 5.0 : -5.0
//        let oldFrame = l.frame
//        let oldOrigin = oldFrame.origin
//        let newOrigin = CGPoint(x: oldOrigin.x + offset, y: oldOrigin.y + offset)
//        let newSize = CGSize(width: oldFrame.size.width + (offset * -2.0), height: oldFrame.height + (offset * -2.0))
//        let newFrame = CGRect(origin: newOrigin, size: newSize)
//        if newFrame.width >= 100.0 && newFrame.width <= 300.0 {
//            l.borderWidth -= offset
//            l.cornerRadius += (offset / 2.0)
//            l.frame = newFrame
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

